/*
 *   Copyright (C) 2012 Philip Silva
 *
 * Permission is hereby granted, free of charge, to any person obtaining 
 * a copy of this software and associated documentation files (the "Software"), 
 * to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 * and/or sell copies of the Software, and to permit persons to whom the 
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 */
package multimin

import (
	"bitbucket.org/mingzhi/goutils/assert"
	"math"
	"testing"
)

type Paraboloid struct {
	center []float64
	scale  []float64
	min    float64
}

func (p Paraboloid) Evaluate(x []float64) float64 {
	return p.scale[0]*math.Pow(x[0]-p.center[0], 2) + p.scale[1]*math.Pow(x[1]-p.center[1], 2) + p.min
}

func (p Paraboloid) DEvaluate(x []float64) (df []float64) {
	df = []float64{
		2 * p.scale[0] * (x[0] - p.center[0]),
		2 * p.scale[1] * (x[1] - p.center[1]),
	}
	return
}

func (p Paraboloid) N() int {
	return 2
}

func TestMinimize(t *testing.T) {
	mTypes := []T{
		GSL_MULTIMIN_FDFMINIMIZER_CONJUGATE_FR,
		GSL_MULTIMIN_FDFMINIMIZER_CONJUGATE_PR,
		GSL_MULTIMIN_FDFMINIMIZER_VECTOR_BFGS2,
		GSL_MULTIMIN_FDFMINIMIZER_VECTOR_BFGS,
	}
	for _, mType := range mTypes {
		testMinimize(t, mType)
	}
}

func testMinimize(t *testing.T, mType T) {
	f := Paraboloid{
		center: []float64{1.0, 2.0},
		scale:  []float64{10.0, 20.0},
		min:    30.0,
	}
	x0 := []float64{5, 7}
	m, err := NewFDFMinimizer(mType, f, x0, 0.01, 1e-4)
	if err != nil {
		t.Fatalf("new fdf minimizer: %v", err)
	}
	defer m.Free()
	for iter := 0; iter < 100; iter++ {
		if err := m.Iterate(); err != nil {
			t.Fatalf("iterate: %v", err)
		}
		done, err := m.TestGradient(1e-3)
		if done {
			t.Logf("Minimum found at:\n")
		}
		t.Logf("%v %.5f %10.5f\n", iter, m.X(), m.FuncValue())
		if err == nil {
			if done {
				if !assert.EqualFloat64(m.FuncValue(), 30, 1e6, 0) {
					t.Errorf("minimum: %.10f, expected 30\n", m.FuncValue())
				}
				if !assert.EqualFloat64(m.X()[0], 1, 1e6, 0) {
					t.Errorf("minimum x[0]: %.10f, expected 1\n", m.X())
				}
				if !assert.EqualFloat64(m.X()[1], 2, 1e6, 0) {
					t.Errorf("minimum x[1]: %.10f, expected 2\n", m.X())
				}
				return
			}
		} else {
			t.Fatalf("test gradient: %v", err)
		}
	}
	t.Fatalf("minimization not successful for %v", mType)
}
